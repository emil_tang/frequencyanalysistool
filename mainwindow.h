﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget* parent = nullptr);
    ~MainWindow();

private slots:
    void on_decryptButton_clicked();
    void on_actionOpen_Reference_File_triggered();
    void on_actionSave_File_triggered();
    void on_actionOpen_Encrypted_File_triggered();

    void on_pushButton_3_clicked();

private:
    Ui::MainWindow* ui;
    QString referenceFile;
    QString encryptFile;
};

#endif // MAINWINDOW_H
