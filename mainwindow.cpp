#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
#include <QStandardPaths>
#include <QTextStream>
#include <algorithm>
#include <array>
#include <map>
#include <string>
#include <vector>

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow() { delete ui; }

auto get_most_common_chars(QString& file)
{
    std::map<QChar, int> count;
    for (auto& c : file) {
        if (c.isLetter())
            count[c]++;
    }
    std::vector<std::pair<QChar, int>> vec;
    std::copy(count.begin(), count.end(),
        std::back_inserter<std::vector<std::pair<QChar, int>>>(vec));
    // Sort by number of word occurences
    std::sort(vec.begin(), vec.end(),
        [](auto& a, auto& b) {
            return a.second < b.second;
        });
    return vec;
}

void MainWindow::on_decryptButton_clicked()
{
    auto reference_chars = get_most_common_chars(referenceFile);
    auto encrypted_chars = get_most_common_chars(encryptFile);
    // create conversion dictionary
    std::map<QChar, QChar> index;
    for (auto enc = encrypted_chars.begin(), ref = reference_chars.begin();
         enc != encrypted_chars.end() && ref != reference_chars.end();
         ++enc, ++ref) {
        index[enc->first] = ref->first;
    }
    // switch characters in string
    QString descryptString("");
    for (auto& c : encryptFile) {
        descryptString.push_back(c.isLetter() ? index[c] : c);
    }
    // write changes
    encryptFile = descryptString;
    ui->encTextEdit->setPlainText(encryptFile);
}

QString open_file(QWidget* parent)
{
    // http://doc.qt.io/qt-5/qtwidgets-tutorials-addressbook-part6-example.html
    QString fileName = QFileDialog::getOpenFileName(parent, "Open File",
        QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation),
        "All files (*)");

    if (fileName.isEmpty()) {
        return QString();
    }
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly)) {
        QMessageBox::information(parent, "Unable to open file", file.errorString());
        return QString();
    }
    QTextStream in(&file);
    return in.readAll();
}

void MainWindow::on_actionOpen_Reference_File_triggered()
{
    referenceFile = open_file(this);
    ui->fileTextEdit->setPlainText(referenceFile);
}

void MainWindow::on_actionSave_File_triggered()
{
    /* Get file name */
    QString fileName = QFileDialog::getSaveFileName(this, "Save File",
        QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation),
        "All Files (*)");
    if (fileName.isEmpty()) {
        return;
    }
    /* Open file as for writing */
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly)) {
        QMessageBox::information(this, "Unable to open file", file.errorString());
        return;
    }
    /* Write data to file */
    QTextStream out(&file);
    out << encryptFile;
}

void MainWindow::on_actionOpen_Encrypted_File_triggered()
{
    encryptFile = open_file(this);
    ui->encTextEdit->setPlainText(encryptFile);
}

void MainWindow::on_pushButton_3_clicked()
{
    QChar a(ui->char1_edit->text().simplified().remove(' ')[0]);
    QChar b(ui->char2_edit->text().simplified().remove(' ')[0]);
    if (a == b) {
        return;
    }
    for (auto& c : encryptFile) {
        if (c == a) {
            c = b;
        } else if (c == b) {
            c = a;
        }
    }
    ui->encTextEdit->setPlainText(encryptFile);
}
